package com.cts.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	/*
	 * @RequestMapping("/") public String greet() {
	 * 
	 * return "login"; // return jsp }
	 * 
	 * @RequestMapping(value = "/login", method = RequestMethod.POST)
	 * 
	 * @ResponseBody public String loginUser(@RequestParam("username") String
	 * username, @RequestParam("password") String userpass) { return username + " "
	 * + userpass; }
	 */
	@RequestMapping("/")
	public String greet() {

		return "login";
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginUser(@RequestParam("username") String username, @RequestParam("password") String userpass,
			Model model) {
		model.addAttribute("username", username);
		return "userhome";
	}
}
